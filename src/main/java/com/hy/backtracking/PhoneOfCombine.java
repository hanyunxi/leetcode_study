package com.hy.backtracking;

import java.util.ArrayList;
import java.util.List;

public class PhoneOfCombine {


    /**
     *  17.电话号码的字母组合
     * 力扣题目链接
     *
     * 给定一个仅包含数字 2-9 的字符串，返回所有它能表示的字母组合。
     *
     * 给出数字到字母的映射如下（与电话按键相同）。注意 1 不对应任何字母。
     *
     */
    List<String> list = new ArrayList<String>();
    //每次迭代获取一个字符串，所以会设计大量的字符串拼接，所以这里选择更为高效的 StringBuild
    StringBuilder temp = new StringBuilder();

    public List<String> letterCombinations(String dig){
        if (dig == null || dig.length() == 0 ){
            return list;
        }
        //初始对应所有的数字，为了直接对应2-9，新增了两个无效的字符串""
        String[] numString = {"", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
        combine(dig,numString,0);
        return list;
    }


    //比如digits如果为"23",num 为0，则str表示2对应的 abc
    public void combine(String dig,String [] numStr,int num){
        //遍历全部一次记录一次得到的字符串
        if (num == dig.length()){
            list.add(temp.toString());
            return;
        }
        // 截取 字符串
        String str = numStr[dig.charAt(num)- '0'] ;
        for (int i = 0; i < str.length(); i++) {
            temp.append(str.charAt(i));
            combine(dig, numStr, num+1);
            //剔除末尾的继续尝试
            temp.deleteCharAt(temp.length() - 1);
        }
    }

    public static void main(String[] args) {
        PhoneOfCombine phoneOfCombine = new PhoneOfCombine();
        System.out.println("res"+phoneOfCombine.letterCombinations("235"));


    }
}
