package com.hy.backtracking;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class SumOfCombine {

    /**
     * 216.组合总和III
     * 力扣题目链接
     *
     * 找出所有相加之和为 n 的 k 个数的组合。组合中只允许含有 1 - 9 的正整数，并且每种组合中不存在重复的数字。
     *
     * 说明：
     *
     * 所有数字都是正整数。
     * 解集不能包含重复的组合。
     * 示例 1: 输入: k = 3, n = 7 输出: [[1,2,4]]
     *
     * 示例 2: 输入: k = 3, n = 9 输出: [[1,2,6], [1,3,5], [2,3,4]]
     *
     * @param
     */
    List<List<Integer>> res = new ArrayList<>();
    LinkedList<Integer> path = new LinkedList<>();
    public List<List<Integer>> combine(int targetSum,int n,int k){
        helper(targetSum,k,1,0);
        return res;
    }

    public void helper(int targetSum,int k,int startIndex,int sum){
        // 剪枝
        if (targetSum < sum){
            return;
        }
        if (path.size() == k){
            // 如果 目标值 == sum  的话
            if (targetSum == sum){
                res.add(new ArrayList<>(path));
            }
            return;
        }
        // 减枝 9 - (k - path.size()) + 1
        int x = 9 - (k - path.size()) + 1 ;
        for (int i = startIndex; i <= x; i++) {
            path.add(i);
            sum += i;
            helper(targetSum,k,i+1,sum);
            // 回溯
            path.removeLast();
            // 回溯
            sum -= i;
        }


    }
    public static void main(String[] args) {
        SumOfCombine sumOfCombine = new SumOfCombine();

        System.out.println("找出所有相加之和为 n 的 k 个数的组合: "+ sumOfCombine.combine(7,9,3) );

    }
}
