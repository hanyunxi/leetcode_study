package com.hy.greedy;

import java.util.Arrays;
import java.util.List;

public class DistributeCandy {


    /**
     *  135. 分发糖果
     * 力扣题目链接
     *
     * 老师想给孩子们分发糖果，有 N 个孩子站成了一条直线，老师会根据每个孩子的表现，预先给他们评分。
     *
     * 你需要按照以下要求，帮助老师给这些孩子分发糖果：
     *
     * 每个孩子至少分配到 1 个糖果。
     * 相邻的孩子中，评分高的孩子必须获得更多的糖果。
     * 那么这样下来，老师至少需要准备多少颗糖果呢？
     *
     * 示例 2:
     *
     * 输入: [1,2,2]
     * 输出: 4
     * 解释: 你可以分别给这三个孩子分发 1、2、1 颗糖果。第三个孩子只得到 1 颗糖果，这已满足上述两个条件。
     *
     * 总结
     * 这在leetcode上是一道困难的题目，其难点就在于贪心的策略，如果在考虑局部的时候想两边兼顾，就会顾此失彼。
     *
     * 那么本题我采用了两次贪心的策略：
     *
     * 一次是从左到右遍历，只比较右边孩子评分比左边大的情况。
     * 一次是从右到左遍历，只比较左边孩子评分比右边大的情况。
     *
     * @param scores
     * @return
     */
    public int distributeCandies(int [] scores){
        int [] candies = new int[scores.length];
        candies[0] = 1;
        /**
         分两个阶段
         1、起点下标1 从左往右，只要 右边 比 左边 大，右边的糖果=左边 + 1
         2、起点下标 ratings.length - 2 从右往左， 只要左边 比 右边 大，此时 左边的糖果应该 取本身的糖果数（符合比它左边大） 和 右边糖果数 + 1 二者的最大值，
            这样才符合 它比它左边的大，也比它右边大
         */
        // 1. 左边 比  右边大
        for (int i = 1; i < scores.length; i++) {
            if (scores[i] > scores[i - 1]){
                candies[i] = candies[i - 1] + 1;
            }else {
                candies[i] = 1;
            }
        }
        // 2.右边 比 左边大
        for (int i = scores.length - 2; i >= 0; i--) {
            if (scores[i] > scores[i+1]){
                // 正序、倒序  数据整合  取最优。
                candies[i] = Math.max(candies[i],candies[i+1] +1);
            }
        }
        return Arrays.stream(candies).sum();
    }


    public static void main(String[] args) {
        int [] score = {5,3,5,2,5,1};
        DistributeCandy distributeCandy = new DistributeCandy();
        System.out.println("res: "+distributeCandy.distributeCandies(score));
    }
}
