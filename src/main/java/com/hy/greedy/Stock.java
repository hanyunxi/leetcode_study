package com.hy.greedy;

public class Stock {


    /**
     * 122.买卖股票的最佳时机II
     * 力扣题目链接
     *
     * 给定一个数组，它的第 i 个元素是一支给定股票第 i 天的价格。
     *
     * 设计一个算法来计算你所能获取的最大利润。你可以尽可能地完成更多的交易（多次买卖一支股票）。
     *
     * 注意：你不能同时参与多笔交易（你必须在再次购买前出售掉之前的股票）。
     *
     * 示例 1:
     *
     * 输入: [7,1,5,3,6,4]
     * 输出: 7
     * 解释: 在第 2 天（股票价格 = 1）的时候买入，在第 3 天（股票价格 = 5）的时候卖出, 这笔交易所能获得利润 = 5-1 = 4。
     * 随后，在第 4 天（股票价格 = 3）的时候买入，在第 5 天（股票价格 = 6）的时候卖出, 这笔交易所能获得利润 = 6-3 = 3 。
     *
     * @param num
     * @return
     */

    // 贪心算法
    public int saleStock(int [] num){
        int profit = 0;
        int diff = 0;

        for (int i = 1; i < num.length; i++) {
/*            diff = num[i] - num[i-1];
            if (diff > 0){
                profit += diff;
            }*/
            profit += Math.max(num[i] - num[i-1],0);
        }
        return profit;
    }


    // DP
    public int maxProfit(int [] num){
        // [天数][是否持有股票]
        // dp[i][1]第i天持有的最多现金
        // dp[i][0]第i天持有股票后的最多现金
        int [][] dp = new int[num.length][2];

        dp[0][0] = 0;
        // 持股票
        dp[0][1] -= num[0];
        for (int i = 1; i < num.length; i++) {
            // dp公式
            // 第i天持股票所剩最多现金 = max(第i-1天持股票所剩现金, 第i-1天持现金-买第i天的股票)
            dp[i][0] = Math.max(dp[i - 1][0],dp[i-1][1] + num[i]);
            // 第i天持有最多现金 = max(第i-1天持有的最多现金，第i-1天持有股票的最多现金+第i天卖出股票)
            dp[i][1] = Math.max(dp[i - 1][1],dp[i-1][0] - num[i]);
            System.out.println(dp[i][0]+"    "+dp[i][1]);
        }
        return dp[num.length - 1][0];
    }

    public static void main(String[] args) {
        Stock stock = new Stock();
        int [] num = {7,1,5,3,6,4};
        System.out.println("res: "+stock.saleStock(num));
        System.out.println("res: "+stock.maxProfit(num));

    }
}
