package com.hy.greedy;

public class Lemonade {


    /**
     * 860.柠檬水找零
     * 力扣题目链接
     *
     * 在柠檬水摊上，每一杯柠檬水的售价为 5 美元。
     *
     * 顾客排队购买你的产品，（按账单 bills 支付的顺序）一次购买一杯。
     *
     * 每位顾客只买一杯柠檬水，然后向你付 5 美元、10 美元或 20 美元。你必须给每个顾客正确找零，也就是说净交易是每位顾客向你支付 5 美元。
     *
     * 注意，一开始你手头没有任何零钱。
     *
     * 如果你能给每位顾客正确找零，返回 true ，否则返回 false 。
     *
     * 示例 1：
     *
     * 输入：[5,5,5,10,20]
     * 输出：true
     * 解释：
     * 前 3 位顾客那里，我们按顺序收取 3 张 5 美元的钞票。
     * 第 4 位顾客那里，我们收取一张 10 美元的钞票，并返还 5 美元。
     * 第 5 位顾客那里，我们找还一张 10 美元的钞票和一张 5 美元的钞票。
     * 由于所有客户都得到了正确的找零，所以我们输出 true。
     *
     * 思路
     * 这是前几天的leetcode每日一题，感觉不错，给大家讲一下。
     *
     * 这道题目刚一看，可能会有点懵，这要怎么找零才能保证完整全部账单的找零呢？
     *
     * 但仔细一琢磨就会发现，可供我们做判断的空间非常少！
     *
     * 只需要维护三种金额的数量，5，10和20。
     *
     * 有如下三种情况：
     *
     * 情况一：账单是5，直接收下。
     * 情况二：账单是10，消耗一个5，增加一个10
     * 情况三：账单是20，优先消耗一个10和一个5，如果不够，再消耗三个5
     *
     * @param bills
     * @return
     */
    public boolean payForLemonade(int [] bills){
        int five = 0,ten=0,twenty = 0;
        for (int bill : bills) {
            // 5
            if (bill == 5){
                five ++;
            }else if (bill == 10 && five > 0){
                ten ++;
                five --;
            }else  if (bill == 20 && five > 0 && ten >0){
                twenty ++;
                five --;
                ten --;
            }else if (bill == 20 && five >= 3){
                five -=3;
                twenty++;
            }else {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Lemonade lemonade = new Lemonade();
        int [] bills = {5,5,10,10,20};
        System.out.println("res: "+ lemonade.payForLemonade(bills));

    }
}
