package com.hy.greedy;

import java.util.Arrays;

public class BalloonShooting {


    /**
     * 452. 用最少数量的箭引爆气球
     * 力扣题目链接
     *
     * 在二维空间中有许多球形的气球。对于每个气球，提供的输入是水平方向上，气球直径的开始和结束坐标。由于它是水平的，所以纵坐标并不重要，因此只要知道开始和结束的横坐标就足够了。开始坐标总是小于结束坐标。
     *
     * 一支弓箭可以沿着 x 轴从不同点完全垂直地射出。在坐标 x 处射出一支箭，若有一个气球的直径的开始和结束坐标为 xstart，xend， 且满足  xstart ≤ x ≤ xend，则该气球会被引爆。可以射出的弓箭的数量没有限制。 弓箭一旦被射出之后，可以无限地前进。我们想找到使得所有气球全部被引爆，所需的弓箭的最小数量。
     *
     * 给你一个数组 points ，其中 points [i] = [xstart,xend] ，返回引爆所有气球所必须射出的最小弓箭数。
     *
     *  思路
     * 1.如何使用最少的弓箭呢？
     *
     * 直觉上来看，貌似只射重叠最多的气球，用的弓箭一定最少，那么有没有当前重叠了三个气球，我射两个，留下一个和后面的一起射这样弓箭用的更少的情况呢？
     *
     * 尝试一下举反例，发现没有这种情况。
     *
     * 那么就试一试贪心吧！局部最优：当气球出现重叠，一起射，所用弓箭最少。全局最优：把所有气球射爆所用弓箭最少。
     *
     *      ”算法确定下来了，那么如何模拟气球射爆的过程呢？是在数组中移除元素还是做标记呢？”
     *
     * 如果真实的模拟射气球的过程，应该射一个，气球数组就remove一个元素，这样最直观，毕竟气球被射了。
     *
     * 但仔细思考一下就发现：如果把气球排序之后，从前到后遍历气球，被射过的气球仅仅跳过就行了，没有必要让气球数组remote气球，只要记录一下箭的数量就可以了。
     *
     * 以上为思考过程，已经确定下来使用贪心了，那么开始解题。
     *
     *      “为了让气球尽可能的重叠，需要对数组进行排序。”
     *
     * 2.那么按照气球起始位置排序，还是按照气球终止位置排序呢？
     *
     * 其实都可以！只不过对应的遍历顺序不同，我就按照气球的起始位置排序了。
     *
     * 既然按照起始位置排序，那么就从前向后遍历气球数组，靠左尽可能让气球重复。
     *
     * 从前向后遍历遇到重叠的气球了怎么办？
     *
     *      “如果气球重叠了，重叠气球中右边边界的最小值 之前的区间一定需要一个弓箭。”
     *
     * 以题目示例： [[10,16],[2,8],[1,6],[7,12]]为例，如图：（方便起见，已经排序）
     * @param balloon
     * @return
     */
    //时间复杂度 : O(NlogN)  排序需要 O(NlogN) 的复杂度
    //
    //空间复杂度 : O(logN) java所使用的内置函数用的是快速排序需要 logN 的空间
    public int balloonShooting(int [][] balloon){
        if (balloon.length == 0 ){
            return 0;
        }
        Arrays.sort(balloon,(x,y) -> Integer.compare(x[0],y[0]));
        int count = 1;
        //重叠气球的最小右边界
        int left = balloon[0][1];
        for (int i = 1; i < balloon.length; i++) {
            //如果下一个气球的左边界大于最小右边界
            // 和 左边界比
            if (balloon[i][0] > left) {
                // 增加 一次射击
                count ++;
                left = balloon[i][1];
                //不然就更新最小右边界
            }
            // 否则和 有边界比     取小值进行比较
            else {
                left = Math.min(left,balloon[i][1]);
            }
        }
        return count;
    }

    public static void main(String [] args) {
        int [][] balloon = {{10,16},{2,8},{1,6},{7,12}};

        BalloonShooting balloonShooting = new BalloonShooting();
        System.out.println("res: "+balloonShooting.balloonShooting(balloon));
    }
}
