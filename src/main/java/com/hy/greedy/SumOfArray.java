package com.hy.greedy;

import java.util.Arrays;
import java.util.stream.IntStream;

public class SumOfArray {

    /**
     * 1005.K次取反后最大化的数组和
     * 力扣题目链接
     *
     * 给定一个整数数组 A，我们只能用以下方法修改该数组：我们选择某个索引 i 并将 A[i] 替换为 -A[i]，然后总共重复这个过程 K 次。（我们可以多次选择同一个索引 i。）
     *
     * 以这种方式修改数组后，返回数组可能的最大和。
     *
     * 示例 3：
     *
     * 输入：A = [2,-3,-1,5,-4], K = 2
     * 输出：13
     * 解释：选择索引 (1, 4) ，然后 A 变为 [2,3,-1,5,4]。
     *
     * 思路
     * 那么本题的解题步骤为：
     *
     * 第一步：将数组按照绝对值大小从大到小排序，注意要按照绝对值的大小
     * 第二步：从前向后遍历，遇到负数将其变为正数，同时K--
     * 第三步：如果K还大于0，那么反复转变数值最小的元素，将K用完
     * 第四步：求和
     * @param nums
     * @param K
     * @return
     */
    public int largestSumAfterKNegations2(int[] nums, int K) {
        // 将数组按照绝对值大小从大到小排序，注意要按照绝对值的大小
        nums = IntStream.of(nums)
                .boxed()
                .sorted((o1, o2) -> Math.abs(o2) - Math.abs(o1))
                .mapToInt(Integer::intValue).toArray();
        int len = nums.length;
        for (int i = 0; i < len; i++) {
            //从前向后遍历，遇到负数将其变为正数，同时K--
            if (nums[i] < 0 && K > 0) {
                nums[i] = -nums[i];
                K--;
            }
        }
        // 如果K还大于0，那么反复转变数值最小的元素，将K用完
        if (K % 2 == 1){
            nums[len - 1] = -nums[len - 1];
        }
        return Arrays.stream(nums).sum();

    }

    public int largestSumAfterKNegations(int [] num,int k){
        if (num.length == 1) {
            return k%2 == 0 ? num[0] : -num[0];
        }
        Arrays.sort(num);
        int idx = 0;
        for (int i = 0; i < k; i++) {
            if (i < num.length - 1 && num[idx] < 0){
                num[idx] = - num[idx];
                if (num[idx] >= Math.abs(num[idx + 1])){
                    idx ++;
                }
                continue;
            }
            // 非 0 第一个 取反
            num[idx] = -num[idx];
        }
        return Arrays.stream(num).sum();
    }

    public static void main(String[] args) {
        int [] num = {2,-3,-1,5,-4};
        int [] num2 = {2,-3,-1,5,-4};
        SumOfArray sumOfArray = new SumOfArray();
        System.out.println("res 1: "+sumOfArray.largestSumAfterKNegations(num,3));
        System.out.println("res 2: "+sumOfArray.largestSumAfterKNegations2(num2,3));
    }
}
