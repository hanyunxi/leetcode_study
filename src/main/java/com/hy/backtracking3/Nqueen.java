package com.hy.backtracking3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Nqueen {


    /**
     * 第51题. N皇后
     * 力扣题目链接
     *
     * n 皇后问题 研究的是如何将 n 个皇后放置在 n×n 的棋盘上，并且使皇后彼此之间不能相互攻击。
     *
     * 给你一个整数 n ，返回所有不同的 n 皇后问题 的解决方案。
     *
     * 每一种解法包含一个不同的 n 皇后问题 的棋子放置方案，该方案中 'Q' 和 '.' 分别代表了皇后和空位。
     * @return
     */
    List<List<Character>> res = new ArrayList<>();
    List<String> path = new ArrayList<>();

    public List<List<Character>> solveNQueens(int n) {
        char[][] chessboard = new char[n][n];
        // 填充数据
        for (char[] c : chessboard) {
            Arrays.fill(c, '.');
        }
        backTrack(n, 0, chessboard);
        return res;
    }

    public void backTrack(int n,int row,char[][]chessboard){
        if (n == row){
            res.add(Array2List(chessboard));
            return;
        }
        for (int col = 0; col < n; col++) {
            // 判断 是否为 queen
            if (isQueen(row,col,chessboard,n)){
                // 放置皇后
                chessboard[row][col] = 'Q';
                backTrack(n,row + 1,chessboard);
                // 回溯，撤销皇后
                chessboard[row][col] = '.';
            }
        }
    }

    public List Array2List(char[][] chessboard) {
        List<String> list = new ArrayList<>();

        for (char[] c : chessboard) {
            list.add(String.copyValueOf(c));
        }
        return list;
    }
    /**
     * 验证棋盘是否合法
     * 按照如下标准去重：
     *
     * 不能同行
     * 不能同列
     * 不能同斜线 （45度和135度角）
     *
     * @return
     */
    public boolean isQueen(int row,int col,char[][] arr,int n){
        // 检查列
        for (int i = 0; i < row; i++) {
            if (arr[i][col] == 'Q'){
                return false;
            }
        }
        // 检查 45度 是否有皇后
        for (int i = row - 1,j = col -1;i >= 0 && j >= 0 ; i--,j--){
            if (arr[i][j] == 'Q'){
                return false;
            }
        }
        // 检查 135度  是否有皇后
        for (int i = row -1,j = col + 1; i >= 0 && j < n ; i --,j++ ){
            if (arr[i][j] == 'Q'){
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Nqueen nqueen = new Nqueen();
        System.out.println("res: "+nqueen.solveNQueens(4));
    }
}
