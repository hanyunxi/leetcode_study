package com.hy.str;

public class ReplaceSpace {

    /**
     *
     * 05.替换空格
     * 请实现一个函数，把字符串 s 中的每个空格替换成"%20"。
     *
     * 示例 1： 输入：s = "We are happy."
     * 输出："We%20are%20happy."
     *
     *
     * @param s
     */
     //使用一个新的对象，复制 str，复制的过程对其判断，是空格则替换，否则直接复制，类似于数组复制
    public static String replaceSpace(String s){
        if (s == null || s.length() == 0){
            return s;
        }
        char[] ch = s.toCharArray();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < ch.length; i++) {
            // 如果为 空格  则拼接 %20
            if (ch[i] == ' '){
                sb.append("%20");
            }else {
                sb.append(ch[i]);
            }
        }
        return sb.toString();
    }

    // 双指针法
    public static String replaceSpace2(String s){
        if (s == null || s.length() == 0){
            return s;
        }
        StringBuilder sb = new StringBuilder();
        //扩充空间，空格数量2倍
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == ' '){
                sb.append("  ");
            }
        }
        //若是没有空格直接返回
        if (sb.length() == 0){
            return s;
        }
        //有空格情况 定义两个指针
        //左指针：指向原始字符串最后一个位置
       int left =  s.length() -1;
        s += sb.toString();
        //右指针：指向扩展字符串的最后一个位置
        int right = s.length() -1;
        char[] chars = s.toCharArray();
        while(left >= 0){
            // 将元素 往后移  遇到 空格 填充 %02
            if (chars[left] == ' '){
                chars[right--] = '0';
                chars[right--] = '2';
                chars[right] = '%';
            }else {
               chars[right] = chars[left];
            }
            left--;
            right--;
        }
        return new String(chars);
    }

    public static void main(String[] args) {

        String s = "we are happy!";
        System.out.println("res1: "+replaceSpace(s));

        System.out.println("res2: "+replaceSpace2(s));

    }
}
