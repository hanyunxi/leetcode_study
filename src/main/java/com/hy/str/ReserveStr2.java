package com.hy.str;

public class ReserveStr2 {

    /**
     *
     * 541. 反转字符串II
     * 力扣题目链接
     *
     * 给定一个字符串 s 和一个整数 k，你需要对从字符串开头算起的每隔 2k 个字符的前 k 个字符进行反转。
     * (每隔2k个  翻转前k个)
     * 如果剩余字符少于 k 个，则将剩余字符全部反转。
     *
     * 如果剩余字符小于 2k 但大于或等于 k 个，则反转前 k 个字符，其余字符保持原样。
     *
     * 示例:
     *
     * 输入: s = "abcdefg", k = 2
     * 输出: "bacdfeg"
     *
     * 思路
     * 这道题目其实也是模拟，实现题目中规定的反转规则就可以了。
     *
     * 一些同学可能为了处理逻辑：每隔2k个字符的前k的字符，写了一堆逻辑代码或者再搞一个计数器，来统计2k，再统计前k个字符。
     *
     * 其实在遍历字符串的过程中，只要让 i += (2 * k)，i 每次移动 2 * k 就可以了，然后判断是否需要有反转的区间。
     *
     * 因为要找的也就是每2 * k 区间的起点，这样写，程序会高效很多。
     *
     * @param s
     */
    //解法二（似乎更容易理解点）
    //题目的意思其实概括为 每隔2k个反转前k个，尾数不够k个时候全部反转
    public static void reverseString(String s ,int k){
        char[] chars = s.toCharArray();
        // 2k个翻转k个字符  0,1   4,5  8，9交换
        for (int i = 0; i < chars.length; i+= 2*k) {
            int start = i;
            int end = Math.min(chars.length-1,start + k -1);
            //用异或运算反转
            while (start < end){
                chars[start] ^= chars[end];
                chars[end] ^= chars[start];
                chars[start] ^= chars[end];
                start ++;
                end --;
            }
        }
        System.out.println("res: "+new String(chars));
    }

    // 数值交换
    public static void reverseString2(String s,int k){
        char[] chars = s.toCharArray();
        // 1. 每隔 2k 个字符的前 k 个字符进行反转
        for (int i = 0; i < chars.length; i+= 2 * k) {
            // 2. 剩余字符小于 2k 但大于或等于 k 个，则反转前 k 个字符
            if (i+k < chars.length){
                reverse(chars,i,i+k-1);
                continue;
            }
            // 3. 剩余字符少于 k 个，则将剩余字符全部反转
            reverse(chars,i,chars.length -1);
        }
        System.out.println("res: "+new String(chars));
    }

    // 定义翻转函数
    public static void reverse(char[] ch, int i, int j) {
        for (; i < j; i++, j--) {
            char temp = ch[i];
            ch[i] = ch[j];
            ch[j] = temp;
        }
    }

    public static void main(String[] args) {
//        String c = "hello";
        String c = "abcdefg";
        reverseString(c,2);
        reverseString2(c,2);
    }
}
