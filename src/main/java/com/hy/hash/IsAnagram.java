package com.hy.hash;

public class IsAnagram {


    /**
     * 哈希表：可以拿数组当哈希表来用，但哈希值不要太大
     * 242.有效的字母异位词
     *
     * 给定两个字符串 s 和 t ，编写一个函数来判断 t 是否是 s 的字母异位词。
     *
     * 示例 1: 输入: s = "anagram", t = "nagaram" 输出: true
     *
     * 示例 2: 输入: s = "rat", t = "car" 输出: false
     *
     * 字母异位词的含义，简单理解为：两字符串长度相同，字母相同，但顺序不同。
     * 说明: 你可以假设字符串只包含小写字母。
     *
     *  思路
     * 先看暴力的解法，两层for循环，同时还要记录字符是否重复出现，很明显时间复杂度是 O(n^2)。
     *
     * 暴力的方法这里就不做介绍了，直接看一下有没有更优的方式。
     *
     * 数组其实就是一个简单哈希表，而且这道题目中字符串只有小写字符，那么就可以定义一个数组，来记录字符串s里字符出现的次数。
     *
     * 如果对哈希表的理论基础关于数组，set，map不了解的话可以看这篇：关于哈希表，你该了解这些！
     *
     * 需要定义一个多大的数组呢，定一个数组叫做record，大小为26 就可以了，初始化为0，因为字符a到字符z的ASCII也是26个连续的数值。
     *
     * 为了方便举例，判断一下字符串s= "aee", t = "eae"。
     *
     */

    public static boolean isAnagram(String s,String t){

        int [] record = new int[26];
        // 遍历 字节 ascii码  +1
        for (char c : s.toCharArray()) {
            record[c - 'a'] += 1;
        }
        // 遍历 字节 ascii码  -1
        for (char c : t.toCharArray()) {
            record[c - 'a'] -= 1;
        }

        for (int i : record) {
            // 只要 数组中  有一个 不为0  则说明 非字母异位词
            if (i != 0){
                return false;
            }
        }
        return true;
    }


    public static void main(String[] args) {
        String s = "bat";
        String t = "tab";

        System.out.println("res : "+ isAnagram(s, t));

    }

}
