package com.hy.hash;

import java.util.HashMap;
import java.util.Map;

public class TwoSum {


    /**
     * 1. 两数之和
     * 给定一个整数数组 nums 和一个目标值 target，请你在该数组中找出和为目标值的那 两个 整数，并返回他们的数组下标。
     * 你可以假设每种输入只会对应一个答案。但是，数组中同一个元素不能使用两遍。
     *
     * 示例:
     * 给定 nums = [2, 7, 11, 15], target = 9
     * 因为 nums[0] + nums[1] = 2 + 7 = 9
     * 所以返回 [0, 1]
     *
     * 思路
     * 很明显暴力的解法是两层for循环查找，时间复杂度是O(n2)。
     * 建议大家做这道题目之前，先做一下这两道
     * 本题呢，则要使用map，那么来看一下使用数组和set来做哈希法的局限。
     *
     * 数组的大小是受限制的，而且如果元素很少，而哈希值太大会造成内存空间的浪费。
     * set是一个集合，里面放的元素只能是一个key，而两数之和这道题目，不仅要判断y是否存在而且还要记录y的下标位置，因为要返回x 和 y的下标。所以set 也不能用。
     *
     * 此时就要选择另一种数据结构：map ，map是一种key value的存储结构，可以用key保存数值，用value在保存数值所在的下标。
     *
     * @return
     */
    public static int[] twoNumSum(int [] num,int target){
        int [] res = new int[2];
        if (num == null || num.length == 0){
            return res;
        }
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < num.length; i++) {
            int temp = target - num[i];
            // 后面还有满足的key  则替换
            if (map.containsKey(temp)){
                res[1] = i;
                res[0] = map.get(temp);
            }
            map.put(num[i],i);
        }
        return res;
    }

    public static void main(String[] args) {
        int [] nums = {1,2,8,7,11,10};
        int target = 9;

        int[] res = twoNumSum(nums, target);

        for (int re : res) {
            System.out.println("res: "+ re);
        }
    }
}
