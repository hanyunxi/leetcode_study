package com.hy.hash;

import java.util.HashMap;
import java.util.Map;

public class FourSum {

    /**
     * 第454题.四数相加II
     * 为了使问题简单化，所有的 A, B, C, D 具有相同的长度 N，
     * 且 0 ≤ N ≤ 500 。所有整数的范围在 -2^28 到 2^28 - 1 之间，
     * 最终结果不会超过 2^31 - 1 。
     *
     * 例如:
     * 输入: A = [ 1, 2] B = [-2,-1] C = [-1, 2] D = [ 0, 2] 输出: 2 解释: 两个元组如下:
     *
     * (0, 0, 0, 1) -> A[0] + B[0] + C[0] + D[1] = 1 + (-2) + (-1) + 2 = 0
     * (1, 1, 0, 0) -> A[1] + B[1] + C[0] + D[0] = 2 + (-1) + (-1) + 0 = 0
     *
     *  思路
     * 本题咋眼一看好像和0015.三数之和，0018.四数之和差不多，其实差很多。
     *
     * 本题是使用哈希法的经典题目，而0015.三数之和，0018.四数之和并不合适使用哈希法，
     * 因为三数之和和四数之和这两道题目使用哈希法在不超时的情况下做到对结果去重是很困难的，很有多细节需要处理。
     *
     * 而这道题目是四个独立的数组，只要找到A[i] + B[j] + C[k] + D[l] = 0就可以，
     * 不用考虑有重复的四个元素相加等于0的情况，所以相对于题目18. 四数之和，题目15.三数之和，还是简单了不少！
     *
     * 本题解题步骤：
     *
     * 首先定义 一个unordered_map，key放a和b两数之和，value 放a和b两数之和出现的次数。
     * 遍历大A和大B数组，统计两个数组元素之和，和出现的次数，放到map中。
     * 定义int变量count，用来统计 a+b+c+d = 0 出现的次数。
     * 在遍历大C和大D数组，找到如果 0-(c+d) 在map中出现过的话，就用count把map中key对应的value也就是出现次数统计出来。
     * 最后返回统计值 count 就可以了
     */
    public static int fourSumCount(int[] nums1, int[] nums2, int[] nums3, int[] nums4){
        Map<Integer,Integer> map = new HashMap<>();
        int res = 0;
        int temp = 0;
        //统计两个数组中的元素之和，同时统计出现的次数，放入map
        for (int i : nums1) {
            for (int j : nums2) {
                temp = i + j;
                if (map.containsKey(temp)){
                    map.put(temp,map.get(temp) + 1);
                }else {
                    map.put(temp,1);
                }
            }
        }
        //统计剩余的两个元素的和，在map中找是否存在相加为0的情况，同时记录次数
        for (int i : nums3) {
            for (int j : nums4) {
                temp = i + j;
                // 判断 是否有 temp 相反数
                if (map.containsKey(0 - temp)){
                    res += map.get(0 - temp);
                }
            }
        }
        return res;
    }

    public static void main(String[] args) {
       int [] A = {1, 2};
       int [] B = {-2,-1};
       int [] C = {-1, 2};
       int [] D = { 0, 2};

        int res = fourSumCount(A, B, C, D);
        System.out.println("res: "+ res);


    }
}
