package com.hy.hash;

public class CanConstruct {


    /**
     * 383. 赎金信
     * 力扣题目链接
     *
     * 给定一个赎金信 (ransom) 字符串和一个杂志(magazine)字符串，判断第一个字符串 ransom 能不能由第二个字符串 magazines 里面的字符构成。如果可以构成，返回 true ；否则返回 false。
     *
     * (题目说明：为了不暴露赎金信字迹，要从杂志上搜索各个需要的字母，组成单词来表达意思。杂志字符串中的每个字符只能在赎金信字符串中使用一次。)
     *
     * 注意：
     *
     * 你可以假设两个字符串均只含有小写字母。
     *
     * canConstruct("a", "b") -> false
     * canConstruct("aa", "ab") -> false
     * canConstruct("aa", "aab") -> true
     *
     * 这道题目和242.有效的字母异位词很像，242.有效的字母异位词相当于求 字符串a 和 字符串b
     * 是否可以相互组成 ，而这道题目是求 字符串a能否组成字符串b，而不用管字符串b 能不能组成字符串a。
     *
     * 本题判断第一个字符串ransom能不能由第二个字符串magazines里面的字符构成，但是这里需要注意两点。
     *
     *  第一点“为了不暴露赎金信字迹，要从杂志上搜索各个需要的字母，组成单词来表达意思”  这里说明杂志里面的字母不可重复使用。
     *
     * 第二点 “你可以假设两个字符串均只含有小写字母。” 说明只有小写字母，这一点很重要
     *
     * @param a
     * @param b
     * @return
     */
    public static boolean canConstruct(String a,String b){
        // 定义一个 hash数组
        int [] hash = new int[26];
        for (char c : b.toCharArray()) {
            hash[c - 'a'] += 1;
        }
        for (char c : a.toCharArray()) {
            hash[c - 'a'] -= 1;
        }

        // 如果数组中存在负数，说明ransomNote字符串总存在magazine中没有的字符
        for (int i : hash) {
            if (i < 0){
                return false;
            }
        }
        return true;
    }


    public static void main(String[] args) {
        String a = "hello";
        String b = "helloc";

        System.out.println("res: "+canConstruct(a,b));
    }
}
