package com.hy.dp;

public class FibonacciSequence {


    /**
     * 509. 斐波那契数
     * 力扣题目链接
     *
     * 斐波那契数，通常用 F(n) 表示，形成的序列称为 斐波那契数列 。该数列由 0 和 1
     * 开始，后面的每一项数字都是前面两项数字的和。也就是： F(0) = 0，F(1) = 1 F(n) = F(n - 1) + F(n - 2)，
     * 其中 n > 1 给你n ，请计算 F(n) 。
     * @param n
     * @return
     */

    // 递归
    public int fib(int n){
        if (n < 2){
            return n;
        }
        return fib(n-1) + fib(n-2);
    }


    public int fib2(int n){
        if (n < 2){
            return n;
        }
        int a = 0,b = 1,c = 0;
        for (int i = 1; i <n; i++) {
            c = a + b;
            a = b;
            b = c;
        }
        return c;
    }

    //非压缩状态的版本
    public int fibonaccisSequence(int n){
        if (n < 2){
            return n;
        }
        int [] dp = new int[n+1];
        dp[0] = 0 ;
        dp[1] = 1 ;
        for (int i = 2; i <= n; i++) {
            dp[i] = dp[i - 1] + dp[i - 2];
        }
        return dp[n];
    }


    public static void main(String[] args) {
        FibonacciSequence fibonacciSequence = new FibonacciSequence();
        System.out.println("res: "+fibonacciSequence.fib(10));
        System.out.println("res: "+fibonacciSequence.fib2(10));
        System.out.println("res: "+fibonacciSequence.fibonaccisSequence(10));
    }
}
