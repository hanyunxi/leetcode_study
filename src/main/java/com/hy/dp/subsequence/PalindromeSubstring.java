package com.hy.dp.subsequence;

public class PalindromeSubstring {


    /**
     * 647. 回文子串
     * 力扣题目链接
     *
     * 给定一个字符串，你的任务是计算这个字符串中有多少个回文子串。
     *
     * 具有不同开始位置或结束位置的子串，即使是由相同的字符组成，也会被视作不同的子串。
     *
     * 示例 1：
     *
     * 输入："abc" 输出：3 解释：三个回文子串: "a", "b", "c"
     *
     * 动态规划
     * 动规五部曲：
     *
     * 1.确定dp数组（dp table）以及下标的含义
     * 布尔类型的dp[i][j]：表示区间范围[i,j] （注意是左闭右闭）的子串是否是回文子串，如果是dp[i][j]为true，否则为false。
     *
     * 2.确定递推公式
     * 在确定递推公式时，就要分析如下几种情况。
     *
     * 整体上是两种，就是s[i]与s[j]相等，s[i]与s[j]不相等这两种。
     *
     * 当s[i]与s[j]不相等，那没啥好说的了，dp[i][j]一定是false。
     *
     * 当s[i]与s[j]相等时，这就复杂一些了，有如下三种情况
     *
     * 情况一：下标i 与 j相同，同一个字符例如a，当然是回文子串
     * 情况二：下标i 与 j相差为1，例如aa，也是回文子串
     * 情况三：下标：i 与 j相差大于1的时候，例如cabac，此时s[i]与s[j]已经相同了，我们看i到j区间是不是回文子串就看aba是不是回文就可以了，那么aba的区间就是 i+1 与 j-1区间，这个区间是不是回文就看dp[i + 1][j - 1]是否为true。
     * result就是统计回文子串的数量。
     *
     * 注意这里我没有列出当s[i]与s[j]不相等的时候，因为在下面dp[i][j]初始化的时候，就初始为false。
     *
     * 3.dp数组如何初始化
     * dp[i][j]可以初始化为true么？ 当然不行，怎能刚开始就全都匹配上了。
     *
     * 所以dp[i][j]初始化为false。
     *
     * 4.确定遍历顺序
     * 遍历顺序可有有点讲究了。
     *
     * 首先从递推公式中可以看出，情况三是根据dp[i + 1][j - 1]是否为true，在对dp[i][j]进行赋值true的。
     *
     * dp[i + 1][j - 1] 在 dp[i][j]的左下角，如图：
     * 如果这矩阵是从上到下，从左到右遍历，那么会用到没有计算过的dp[i + 1][j - 1]，也就是根据不确定是不是回文的区间[i+1,j-1]，来判断了[i,j]是不是回文，那结果一定是不对的。
     *
     * 所以一定要从下到上，从左到右遍历，这样保证dp[i + 1][j - 1]都是经过计算的。
     *
     * 有的代码实现是优先遍历列，然后遍历行，其实也是一个道理，都是为了保证dp[i + 1][j - 1]都是经过计算的。
     *
     * @param str
     * @return
     */
    public static int palindromeSubstring(String str){
        int len,res=0;
        if ((len = str.length()) < 1 || str == null){
            return 0;
        }
        //dp[i][j]：s字符串下标i到下标j的字串是否是一个回文串，即s[i, j]
        boolean [][] dp = new boolean[str.length()][str.length()];

        for (int i = 0; i < len; i++) {
            for (int j = 0; j <= i; j++) {
                //当两端字母一样时，才可以两端收缩进一步判断
                if (str.charAt(i) == str.charAt(j)){
                    //i++，j--，即两端收缩之后i,j指针指向同一个字符或者i超过j了,必然是一个回文串
                    if (j - i < 3){
                        dp[i][j] = true;
                    }else {
                        //否则通过收缩之后的字串判断
                        dp[i][j] = dp[i + 1][j - 1];
                    }
                }else {
                    //两端字符不一样，不是回文串
                    dp[i][j] = false;
                }
            }
        }
        //遍历每一个字串，统计回文串个数
        for (int i = 0; i < len; i++) {
            for (int j = 0; j < len; j++) {
                if (dp[i][j]) {
                    res ++;
                }
            }
        }
        return res;
    }

    /**
     * 双指针法    中心扩散法：
     * 动态规划的空间复杂度是偏高的，我们再看一下双指针法。
     *
     * 首先确定回文串，就是找中心然后向两边扩散看是不是对称的就可以了。
     *
     * 在遍历中心点的时候，要注意中心点有两种情况。
     *
     * 一个元素可以作为中心点，两个元素也可以作为中心点。
     *
     * 那么有人同学问了，三个元素还可以做中心点呢。其实三个元素就可以由一个元素左右添加元素得到，四个元素则可以由两个元素左右添加元素得到。
     *
     * 所以我们在计算的时候，要注意一个元素为中心点和两个元素为中心点的情况。
     *
     * 这两种情况可以放在一起计算，但分别计算思路更清晰，我倾向于分别计算，
     *
     *
     * @param str
     * @return
     */
    public static int palindromeSubstring02(String str){
        int len,res = 0;
        if (str == null || (len = str.length()) < 0 ){
            return 0;
        }
        //总共有2 * len - 1个中心点
        for (int i = 0; i < 2 * len + 1; i++) {
            //通过遍历每个回文中心，向两边扩散，并判断是否回文字串
            //有两种情况，left == right，right = left + 1，这两种回文中心是不一样的
            int left = i/2,right=left + i %2;
            while (left >= 0 && right < len && str.charAt(left) == str.charAt(right)){
                //如果当前是一个回文串，则记录数量
                left --;
                right ++;
                res ++;
            }
        }
        return res;
    }

    public static void main(String[] args) {
        String str = "aaa";
        String str2 = "aba";
        System.out.println("res: "+palindromeSubstring(str));
        System.out.println("res: "+palindromeSubstring02(str2));
    }
}
