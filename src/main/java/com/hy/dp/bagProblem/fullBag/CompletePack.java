package com.hy.dp.bagProblem.fullBag;

public class CompletePack {

    /**
     * 完全 背包
     * 1.每件物品都有无限个（也就是可以放入背包多次）
     * 2.纯完全背包问题，其for循环的先后循环是可以颠倒的！
     * @param weight
     * @param value
     * @param bagWeight
     * @return
     */
    //  先遍历  物品  再遍历  背包
    public static int completePack(int [] weight,int [] value,int bagWeight){
        //1.dp数组定义以及下标含义
        int [] dp = new int[bagWeight + 1];
        //2.推导递推式
        //dp[j] = Math.max(dp[j],dp[j - weight[i]] + value[i]);
        //3.初始化
        //4.循环遍历 先遍历  物品  再遍历  背包
        for (int i = 0; i < weight.length; i++) {
            for (int j = weight[i]; j <= bagWeight; j++) {
                dp[j] = Math.max(dp[j],dp[j -weight[i]] + value[i]);
            }
        }
        //5.结果
        return dp[bagWeight];
    }


    public static int completePack02(int [] weight,int [] value,int bagweight){
        int [] dp = new int[bagweight + 1];
        //循环遍历  先遍历背包   再遍历物品
        for (int i = 1; i <= bagweight; i++) {
            for (int j = 0; j < weight.length; j++) {
                if (i - weight[j] >= 0){
                    dp[i] = Math.max(dp[i],dp[i - weight[j]] + value[j]);
                }
            }
        }
        return dp[bagweight];
    }


    public static void main(String[] args) {
        int[] weight = {1, 3, 4};
        int[] value = {15, 20, 30};
        int bagWeight = 4;

        System.out.println("res: "+completePack(weight,value,bagWeight));
        System.out.println("res2: "+completePack02(weight,value,bagWeight));

    }
}
