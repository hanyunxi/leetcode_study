package com.hy.dp.bagProblem.raidHomes;

public class PlunderHouses02 {


    /**
     * 213.打家劫舍II
     * 力扣题目链接
     *
     * 你是一个专业的小偷，计划偷窃沿街的房屋，每间房内都藏有一定的现金。这个地方所有的房屋都 围成一圈 ，这意味着第一个房屋和最后一个房屋是紧挨着的。
     * 同时，相邻的房屋装有相互连通的防盗系统，如果两间相邻的房屋在同一晚上被小偷闯入，系统会自动报警 。
     * 给定一个代表每个房屋存放金额的非负整数数组，计算你 在不触动警报装置的情况下 ，能够偷窃到的最高金额。
     * 示例 1：输入：nums = [2,3,2] 输出：3 解释：你不能先偷窃 1 号房屋（金额 = 2），然后偷窃 3 号房屋（金额 = 2）, 因为他们是相邻的。
     * 示例 2： 输入：nums = [1,2,3,1] 输出：4 解释：你可以先偷窃 1 号房屋（金额 = 1），然后偷窃 3 号房屋（金额 = 3）。偷窃到的最高金额 = 1 + 3 = 4 。
     * 思路
     * 这道题目和198.打家劫舍是差不多的，唯一区别就是成环了。
     * 对于一个数组，成环的话主要有如下三种情况：
     *
     * 情况一：考虑不包含首尾元素
     * 0        1   2   3       4
     * 1        6   1   9       1
     * 情况二：考虑包含首元素，不包含尾元素
     * 0   1   2   3       4
     * 1   6   1   9       1
     * 情况三：考虑包含尾元素，不包含首元素
     * 0        1   2   3   4
     * 1        6   1   9   1
     * @param nums
     * @return
     */
    public static int rob(int [] nums){
        int length = nums.length;
        if (nums == null || length == 0){
            return 0;
        }

        if (length == 1){
            return nums[0];
        }
        return Math.max(robAction(nums,0,length - 1),robAction(nums,1,length));
    }
    public static int robAction(int [] nums,int start,int end){
        int x=0,y=0,z=0;
        for (int i = start; i < end; i++) {
            y = z;
            z = Math.max(y,x+nums[i]);
            x = y;
        }
        return z;
    }

    public static void main(String[] args) {
        int [] nums = {1,6,1,9,1};
        System.out.println("res: "+ rob(nums));
    }
}
