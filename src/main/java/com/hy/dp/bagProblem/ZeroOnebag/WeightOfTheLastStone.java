package com.hy.dp.bagProblem.ZeroOnebag;

import java.util.Arrays;

public class WeightOfTheLastStone {


    /**
     * 1049. 最后一块石头的重量 II
     * 力扣题目链接
     *
     * 题目难度：中等
     *
     * 有一堆石头，每块石头的重量都是正整数。
     *
     * 每一回合，从中选出任意两块石头，然后将它们一起粉碎。假设石头的重量分别为 x 和 y，且 x <= y。那么粉碎的可能结果如下：
     *
     * 如果 x == y，那么两块石头都会被完全粉碎； 如果 x != y，那么重量为 x 的石头将会完全粉碎，而重量为 y 的石头新重量为 y-x。 最后，最多只会剩下一块石头。返回此石头最小的可能重量。如果没有石头剩下，就返回 0。
     *
     * 示例： 输入：[2,7,4,1,8,1] 输出：1 解释： 组合 2 和 4，得到 2，所以数组转化为 [2,7,1,8,1]， 组合 7 和 8，得到 1，所以数组转化为 [2,1,1,1]， 组合 2 和 1，得到 1，所以数组转化为 [1,1,1]， 组合 1 和 1，得到 0，所以数组转化为 [1]，这就是最优值。
     *
     * 思路：
     * 接下来进行动规五步曲：
     *
     * 1.确定dp数组以及下标的含义
     * dp[j]表示容量（这里说容量更形象，其实就是重量）为j的背包，最多可以背dp[j]这么重的石头。
     *
     * 2.确定递推公式
     * 01背包的递推公式为：dp[j] = max(dp[j], dp[j - weight[i]] + value[i]);
     *
     * 本题则是：dp[j] = max(dp[j], dp[j - stones[i]] + stones[i]);
     *
     * 一些同学可能看到这dp[j - stones[i]] + stones[i]中 又有- stones[i] 又有+stones[i]，看着有点晕乎。
     *
     * 还是要牢记dp[j]的含义，要知道dp[j - stones[i]]为 容量为j - stones[i]的背包最大所背重量。
     *
     * 3.dp数组如何初始化
     * 既然 dp[j]中的j表示容量，那么最大容量（重量）是多少呢，就是所有石头的重量和。
     *
     * 因为提示中给出1 <= stones.length <= 30，1 <= stones[i] <= 1000，所以最大重量就是30 * 1000 。
     *
     * 而我们要求的target其实只是最大重量的一半，所以dp数组开到15000大小就可以了。
     *
     * 当然也可以把石头遍历一遍，计算出石头总重量 然后除2，得到dp数组的大小。
     *
     * 4.确定遍历顺序
     * 在动态规划：关于01背包问题，你该了解这些！（滚动数组）中就已经说明：如果使用一维dp数组，物品遍历的for循环放在外层，遍历背包的for循环放在内层，且内层for循环倒序遍历！
     * for (int i = 0; i < stones.size(); i++) { // 遍历物品
     *     for (int j = target; j >= stones[i]; j--) { // 遍历背包
     *         dp[j] = max(dp[j], dp[j - stones[i]] + stones[i]);
     *     }
     * }
     *
     * 5.举例推导dp数组
     * 举例，输入：[2,4,1,1]，此时target = (2 + 4 + 1 + 1)/2 = 4 ，dp数组状态图如下：
     *
     * @param stone
     * @return
     */
    public static int weightOfTheLastStone(int [] stone){
        // 1.确定dp数组以及下标的含义
        int sum = Arrays.stream(stone).sum();
        // 容量
        int target = sum >> 1;
        //初始化dp数组
        int [] dp = new int[target + 1];
        // 循环遍历
        for (int i = 0; i < stone.length; i++) {
            // 采用  倒序 （一维数组）
            for (int j = target; j >= stone[i]; j--) {
                //两种情况，要么放，要么不放
             dp[j] = Math.max(dp[j],dp[j - stone[i]] + stone[i]);
            }
        }
        return sum - 2*dp[target];
    }
    public static void main(String[] args) {
        int [] stone = {2,4,1,1};
        System.out.println("res: "+ WeightOfTheLastStone.weightOfTheLastStone(stone));
    }
}
