package com.hy.dp.bagProblem.ZeroOnebag;

import java.util.Arrays;

public class TargetSum {

    /**
     * 494. 目标和
     * 力扣题目链接
     *
     * 难度：中等
     *
     * 给定一个非负整数数组，a1, a2, ..., an, 和一个目标数，S。现在你有两个符号 + 和 -。对于数组中的任意一个整数，你都可以从 + 或 -中选择一个符号添加在前面。
     *
     * 返回可以使最终数组和为目标数 S 的所有添加符号的方法数。
     * 示例：
     *
     * 输入：nums: [1, 1, 1, 1, 1], S: 3
     * 输出：5
     *
     * 解释：
     * -1+1+1+1+1 = 3
     * +1-1+1+1+1 = 3
     * +1+1-1+1+1 = 3
     * +1+1+1-1+1 = 3
     * +1+1+1+1-1 = 3
     *
     * 一共有5种方法让最终目标和为3。
     *
     * 动态规划
     * 如何转化为01背包问题呢。
     *
     * 假设加法的总和为x，那么减法对应的总和就是sum - x。
     *
     * 所以我们要求的是 x - (sum - x) = S
     *
     * x = (S + sum) / 2
     * 此时问题就转化为，装满容量为x背包，有几种方法。
     *
     * 大家看到(S + sum) / 2 应该担心计算的过程中向下取整有没有影响。
     *
     * 这么担心就对了，例如sum 是5，S是2的话其实就是无解的，所以：
     *
     * if ((S + sum) % 2 == 1) return 0; // 此时没有方案
     * 同时如果 S的绝对值已经大于sum，那么也是没有方案的。
     *
     * if (abs(S) > sum) return 0; // 此时没有方案
     *
     * 再回归到01背包问题，为什么是01背包呢？
     *
     * 因为每个物品（题目中的1）只用一次！
     *
     * 这次和之前遇到的背包问题不一样了，之前都是求容量为j的背包，最多能装多少。
     *
     * 本题则是装满有几种方法。其实这就是一个组合问题了。
     * 1.确定dp数组以及下标的含义
     * dp[j] 表示：填满j（包括j）这么大容积的包，有dp[j]种方法
     *
     * 2.确定递推公式
     * 确定递推公式
     * 有哪些来源可以推出dp[j]呢？
     *
     * 不考虑nums[i]的情况下，填满容量为j的背包，有dp[j]种方法。
     *
     * 那么考虑nums[i]的话（只要搞到nums[i]），凑成dp[j]就有dp[j - nums[i]] 种方法。
     *
     * 例如：dp[j]，j 为5，
     *
     * 已经有一个1（nums[i]） 的话，有 dp[4]种方法 凑成 dp[5]。
     * 已经有一个2（nums[i]） 的话，有 dp[3]种方法 凑成 dp[5]。
     * 已经有一个3（nums[i]） 的话，有 dp[2]中方法 凑成 dp[5]
     * 已经有一个4（nums[i]） 的话，有 dp[1]中方法 凑成 dp[5]
     * 已经有一个5 （nums[i]）的话，有 dp[0]中方法 凑成 dp[5]
     * 那么凑整dp[5]有多少方法呢，也就是把 所有的 dp[j - nums[i]] 累加起来。
     * 所以求组合类问题的公式，都是类似这种：
     *
     * dp[j] += dp[j - nums[i]]
     * 这个公式在后面在讲解背包解决排列组合问题的时候还会用到！
     *
     * 3.dp数组如何初始化
     * 从递归公式可以看出，在初始化的时候dp[0] 一定要初始化为1，因为dp[0]是在公式中一切递推结果的起源，如果dp[0]是0的话，递归结果将都是0。
     *
     * dp[0] = 1，理论上也很好解释，装满容量为0的背包，有1种方法，就是装0件物品。
     *
     * dp[j]其他下标对应的数值应该初始化为0，从递归公式也可以看出，dp[j]要保证是0的初始值，才能正确的由dp[j - nums[i]]推导出来。
     *
     *
     * @param nums
     * @param s
     * @return
     */
    public static int targetOfSum(int [] nums,int s){

        int sum = Arrays.stream(nums).sum();
        if ((sum + s) %2 != 0){
            return 0;
        }
        // 背包 容量
        int target = (sum + s)/2;
        if (target < 0 ){
            target -= target;
        }
        // 1. 确定dp数组以及下标
        int [] dp = new int[target + 1];
        // 2. 推导递推式
        // dp[j] = dp[j - nums[i]];
        //3.初始化
        dp[0] = 1;
        //4. 循环遍历
        // 物品
        for (int i = 0; i < nums.length; i++) {
            // 倒序遍历 背包
            for (int j = target; j >= nums[i]; j--) {
                dp[j] += dp[j - nums[i]];
            }
        }
        return dp[target];
    }


    public static void main(String[] args) {
        int [] nums = {1,1,1,1,1};
        int s = 3;
        int res = targetOfSum(nums, s);
        System.out.println(String.format("一共有%s种方法让最终目标和为%s。", res, s ));
    }
}
