package com.hy.dp.stock;

public class BuySaleStock04 {


    /**
     * 188.买卖股票的最佳时机IV
     * 力扣题目链接
     *
     * 给定一个整数数组 prices ，它的第 i 个元素 prices[i] 是一支给定的股票在第 i 天的价格。
     *
     * 设计一个算法来计算你所能获取的最大利润。你最多可以完成 k 笔交易。
     *
     * 注意：你不能同时参与多笔交易（你必须在再次购买前出售掉之前的股票）。
     *
     * 示例 1： 输入：k = 2, prices = [2,4,1] 输出：2 解释：在第 1 天 (股票价格 = 2) 的时候买入，在第 2 天 (股票价格 = 4) 的时候卖出，这笔交易所能获得利润 = 4-2 = 2。
     *
     * @param prices
     * @param k
     * @return
     */
    public static int maxProfix(int [] prices,int k){

        // [天数][股票状态]
        // 股票状态: 奇数表示第 k 次交易持有/买入, 偶数表示第 k 次交易不持有/卖出, 0 表示没有操作
        int len = prices.length;
        int [][] dp = new int[len][k*2 + 1];

        // dp数组的初始化, 与版本一同理
        for (int i = 1; i < 2 * k ; i+=2) {
            dp[0][i] = -prices[0];
        }

        for (int i = 1; i < len; i++) {
            for (int j = 0; j < 2 * k - 1; j += 2) {
                dp[i][j + 1] = Math.max(dp[i - 1][j + 1],dp[i - 1][j] - prices[i]);
                dp[i][j + 2] = Math.max(dp[i - 1][j + 2],dp[i][j + 1] + prices[i]);
            }
        }
        return dp[len - 1][2 * k];
    }

     //版本三：一维 dp数组
    public static int maxProfix02(int [] prices,int k){
        if (prices.length == 0 || k == 0 ){
            return 0;
        }
        // 其实就是123题的扩展，123题只用记录2次交易的状态
        // 这里记录k次交易的状态就行了
        // 每次交易都有买入，卖出两个状态，所以要乘 2
        int [] dp = new int[2 * k];

        for (int i = 0; i < dp.length/2; i++) {
            dp[i * 2] = -prices[0];
        }

        for (int i = 1; i <= prices.length; i++) {
            dp[0] = Math.max(dp[0],-prices[i - 1]);
            dp[1] = Math.max(dp[1],dp[0] + prices[i - 1]);
            // 还是与123题一样，与123题对照来看
            // 就很容易啦
            for (int j = 2; j < dp.length; j+=2) {
                dp[j] = Math.max(dp[j],dp[j - 1] - prices[i - 1]);
                dp[j + 1] = Math.max(dp[j + 1],dp[j] + prices[i - 1]);
            }
        }

        return dp[dp.length - 1];
    }

    public static void main(String[] args) {
        int [] prices = {3,3,5,0,0,3,1,4};

        System.out.println("res: "+ maxProfix(prices,2));
        System.out.println("res: "+ maxProfix02(prices,2));

    }
}
