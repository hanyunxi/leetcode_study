package com.hy.dp;

public class DifferentPaths {






    // m 3  1   1   1   1
    // n 3  1   2   3   4
    //      1   3   6   10

    /**
     * 62.不同路径
     * 力扣题目链接
     *
     * 一个机器人位于一个 m x n 网格的左上角 （起始点在下图中标记为 “Start” ）。
     *
     * 机器人每次只能向下或者向右移动一步。机器人试图达到网格的右下角（在下图中标记为 “Finish” ）。
     *
     * 问总共有多少条不同的路径？
     * 示例 1：
     * 输入：m = 3, n = 7
     * 输出：28
     *
     * 示例 2：
     * 输入：m = 2, n = 3
     * 输出：3
     *
     * 动态规划
     * 机器人从(0 , 0) 位置出发，到(m - 1, n - 1)终点。
     *  1. 确定dp数组下标含义 dp[i][j] 到每一个坐标可能的路径种类
     *      dp[i][j] ：表示从（0 ，0）出发，到(i, j) 有dp[i][j]条不同的路径。
     *  2. 递推公式 dp[i][j] = dp[i-1][j] dp[i][j-1]
     *      想要求dp[i][j]，只能有两个方向来推导出来，即dp[i - 1][j] 和 dp[i][j - 1]。
     *      此时在回顾一下 dp[i - 1][j] 表示啥，是从(0, 0)的位置到(i - 1, j)有几条路径，dp[i][j - 1]同理。
     *      那么很自然，dp[i][j] = dp[i - 1][j] + dp[i][j - 1]，因为dp[i][j]只有这两个方向过来。
     *  3. 初始化 dp[i][0]=1 dp[0][i]=1 初始化横竖就可
     *      首先dp[i][0]一定都是1，因为从(0, 0)的位置到(i, 0)的路径只有一条，那么dp[0][j]也同理。 所以初始化代码为：
     *  4. 遍历顺序 一行一行遍历
     *      这里要看一下递归公式dp[i][j] = dp[i - 1][j] + dp[i][j - 1]，dp[i][j]都是从其上方和左方推导而来，那么从左到右一层一层遍历就可以了。
     *      这样就可以保证推导dp[i][j]的时候，dp[i - 1][j] 和 dp[i][j - 1]一定是有数值的。
     *  5. 推导结果 。。。。。。。。
     *
     * @param m
     * @param n
     * @return
     */
    public int differentPaths(int m,int n){
        int [][] dp = new int[m][n];
        // 初始化 m
        for (int i = 0; i < m; i++) {
            dp[i][0] = 1;
        }
        // 初始化 n
        for (int i = 0; i < n; i++) {
            dp[0][i] = 1;
        }
         // 逐层 遍历
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
            }
        }
        return dp[m-1][n-1];
    }


    public static void main(String[] args) {
        DifferentPaths differentPaths = new DifferentPaths();
        int m = 3;
        int n = 7;
        System.out.println("res: "+differentPaths.differentPaths(3,7));
    }
}
