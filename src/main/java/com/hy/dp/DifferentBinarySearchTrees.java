package com.hy.dp;

public class DifferentBinarySearchTrees {


    /**
     * 96.不同的二叉搜索树
     * 力扣题目链接
     *
     * 给定一个整数 n，求以 1 ... n 为节点组成的二叉搜索树有多少种？
     *
     * 示例:
     *
     * dp[3]，就是 元素1为头结点搜索树的数量 + 元素2为头结点搜索树的数量 + 元素3为头结点搜索树的数量
     *
     * 元素1为头结点搜索树的数量 = 右子树有2个元素的搜索树数量 * 左子树有0个元素的搜索树数量
     *
     * 元素2为头结点搜索树的数量 = 右子树有1个元素的搜索树数量 * 左子树有1个元素的搜索树数量
     *
     * 元素3为头结点搜索树的数量 = 右子树有0个元素的搜索树数量 * 左子树有2个元素的搜索树数量
     *
     * 有2个元素的搜索树数量就是dp[2]。
     *
     * 有1个元素的搜索树数量就是dp[1]。
     *
     * 有0个元素的搜索树数量就是dp[0]。
     *
     * 所以dp[3] = dp[2] * dp[0] + dp[1] * dp[1] + dp[0] * dp[2]
     *
     * 确定dp数组（dp table）以及下标的含义
     * dp[i] ： 1到i为节点组成的二叉搜索树的个数为dp[i]。
     *
     * 也可以理解是i的不同元素节点组成的二叉搜索树的个数为dp[i] ，都是一样的。
     *
     * 以下分析如果想不清楚，就来回想一下dp[i]的定义
     *
     * 确定递推公式
     * 在上面的分析中，其实已经看出其递推关系， dp[i] += dp[以j为头结点左子树节点数量] * dp[以j为头结点右子树节点数量]
     *
     * j相当于是头结点的元素，从1遍历到i为止。
     *
     * 所以递推公式：dp[i] += dp[j - 1] * dp[i - j]; ，j-1 为j为头结点左子树节点数量，i-j 为以j为头结点右子树节点数量
     *
     * dp数组如何初始化
     * 初始化，只需要初始化dp[0]就可以了，推导的基础，都是dp[0]。
     *
     * 那么dp[0]应该是多少呢？
     *
     * 从定义上来讲，空节点也是一棵二叉树，也是一棵二叉搜索树，这是可以说得通的。
     *
     * 从递归公式上来讲，dp[以j为头结点左子树节点数量] * dp[以j为头结点右子树节点数量] 中以j为头结点左子树节点数量为0，也需要dp[以j为头结点左子树节点数量] = 1， 否则乘法的结果就都变成0了。
     *
     * 所以初始化dp[0] = 1
     *
     * 确定遍历顺序
     * 首先一定是遍历节点数，从递归公式：dp[i] += dp[j - 1] * dp[i - j]可以看出，节点数为i的状态是依靠 i之前节点数的状态。
     *
     * 那么遍历i里面每一个数作为头结点的状态，用j来遍历。
     *
     * 代码如下：
     *
     * for (int i = 1; i <= n; i++) {
     *     for (int j = 1; j <= i; j++) {
     *         dp[i] += dp[j - 1] * dp[i - j];
     *     }
     * }
     * 举例推导dp数组
     * n为5时候的dp数组状态如图：
     * 0    1   2   3   4   5
     * 1    1   2   5   14  42
     * @param n
     * @return
     */
    public int differentBinarySearchTrees(int n){
        //初始化 dp 数组
        int [] dp = new int[n + 1];
        //初始化0个节点和1个节点的情况
        dp[0] = 1;
        dp[1] = 1;
        for (int i = 2; i <= n; i++) {
            for (int j = 1; j <= i; j++) {
                //对于第i个节点，需要考虑1作为根节点直到i作为根节点的情况，所以需要累加
                //一共i个节点，对于根节点j时,左子树的节点个数为j-1，右子树的节点个数为i-j
                dp[i] += dp[j - 1] * dp[i-j];
            }
        }
        return dp[n];
    }

    public static void main(String[] args) {
        DifferentBinarySearchTrees differentBinarySearchTrees = new DifferentBinarySearchTrees();
        System.out.println("res: "+differentBinarySearchTrees.differentBinarySearchTrees(3));
    }
}
