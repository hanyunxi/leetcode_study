package com.hy.array;

public class RemoveElement {
    /**
     * 27. 移除元素
     *  https://leetcode.cn/problems/remove-element/
     * 给你一个数组 nums 和一个值 val，你需要 原地 移除所有数值等于 val 的元素，并返回移除后数组的新长度。
     *
     * 示例 2: 给定 nums = [0,1,2,2,3,0,4,2], val = 2, 函数应该返回新的长度 5,
     * 并且 nums 中的前五个元素为 0, 1, 3, 0, 4。
     */

    // 快慢指针
    public static  int removeElement(int [] nums,int val){
        int fastIndex = 0,slowIndex = 0;
        for (;fastIndex < nums.length; fastIndex++) {
            if (nums[fastIndex] != val) {
                nums[slowIndex] = nums[fastIndex];
                slowIndex++;
            }
        }
        return slowIndex;
    }

    public static void main(String[] args) {
        int [] nums = {0,1,2,2,3,0,4,2};
        int val = 2;
        System.out.println("len : "+removeElement(nums, val));

    }
}
