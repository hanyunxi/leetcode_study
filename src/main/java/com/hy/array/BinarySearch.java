package com.hy.array;

public class BinarySearch {
    /**
     * 704. 二分查找
     *
     * 给定一个 n 个元素有序的（升序）整型数组 nums 和一个目标值 target  ，
     * 写一个函数搜索 nums 中的 target，如果目标值存在返回下标，否则返回 -1。
     *
     * 示例 1:
     * 输入: nums = [-1,0,3,5,9,12], target = 9
     * 输出: 4
     * 解释: 9 出现在 nums 中并且下标为 4
     */

    /**
     * 第一种写法，我们定义 target 是在一个在左闭右闭的区间里，也就是[left, right] （这个很重要非常重要）。
     *
     * 区间的定义这就决定了二分法的代码应该如何写，因为定义target在[left, right]区间，所以有如下两点：
     * while (left <= right) 要使用 <= ，因为left == right是有意义的，所以使用 <=
     * if (nums[middle] > target) right 要赋值为 middle - 1，因为当前这个nums[middle]一定不是target，
     * 那么接下来要查找的左区间结束下标位置就是 middle - 1
     */
    // （版本一）左闭右闭区间
    public static int search_one(int[] nums, int target){
        // 避免当 target 小于nums[0] nums[nums.length - 1] 时多次循环运算
        if (target < nums[0] || target > nums[nums.length - 1]){
            return -1;
        }
        int left = 0,right = nums.length -1;

        while (left <= right){
            int mid = left + ((right - left) >> 1);
            if (nums[mid] == target){
                return mid;
            }else if (nums[mid] < target){
                left =  mid + 1;
            }else if (nums[mid] > target){
                right =  mid - 1;
            }
        }
        return -1;
    }

    public static int search_two(int[] nums,int target){
        int left = 0,right = nums.length;

        while (left < right){
            int mid = left + ((right - left) >> 1);
            if (nums[mid] == target){
                return mid;
            }else if (nums[mid] < target){
                left = mid + 1;
            }else if (nums[mid] > target){
                right = mid;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        int [] nums = {-1,0,3,5,9,12};
        int target = 9;
//        System.out.println(search_one(nums,target));
        System.out.println(search_two(nums,target));
    }

}
