package com.hy.array;

public class MinSubArrayLen {

    /**
     * 209.长度最小的子数组
     *
     * 给定一个含有 n 个正整数的数组和一个正整数 s ，找出该数组中满足其和 ≥ s 的长度最小的 连续 子数组，
     * 并返回其长度。如果不存在符合条件的子数组，返回 0。
     *  滑动窗口
     *  所谓滑动窗口，就是不断的调节子序列的起始位置和终止位置，从而得出我们要想的结果。
     *
     *  在本题中实现滑动窗口，主要确定如下三点：
     *
     *  窗口内是什么？
     *  如何移动窗口的起始位置？
     *  如何移动窗口的结束位置？
     *  窗口就是 满足其和 ≥ s 的长度最小的 连续 子数组。
     *
     *  窗口的起始位置如何移动：如果当前窗口的值大于s了，窗口就要向前移动了（也就是该缩小了）。
     *
     *  窗口的结束位置如何移动：窗口的结束位置就是遍历数组的指针，也就是for循环里的索引。
     * @param nums
     * @return
     */
    // 滑动窗口
    public static int minSubArrayLen(int [] nums,int s){
        int left = 0,sum=0,result=Integer.MAX_VALUE;
        for (int right = 0;right<nums.length;right++){
            sum += nums[right];
            // 一旦发现子序列和超过了s，更新result
            while (sum >= s){
                // 取子序列的长度
                result = Math.min(result,right - left + 1);
                // 减去数组 第left个元素 （连续）
                sum -= nums[left++];
            }
        }
        return result == Integer.MAX_VALUE ? 0 : result;
    }

    public static void main(String[] args) {
        int s = 7;
        int [] nums = {2,3,1,2,4,3};
        System.out.println("len: "+minSubArrayLen(nums, s));

    }
}
