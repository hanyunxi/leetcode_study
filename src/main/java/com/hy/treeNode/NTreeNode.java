package com.hy.treeNode;

import java.util.List;

public class NTreeNode {

    int val;
    public List<NTreeNode> child;

    public NTreeNode() {

    }

    public NTreeNode(int val) {
        this.val = val;
    }

    public NTreeNode(int val, List<NTreeNode> child) {
        this.val = val;
        this.child = child;
    }
}
