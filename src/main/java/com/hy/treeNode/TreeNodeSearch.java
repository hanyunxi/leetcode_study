package com.hy.treeNode;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TreeNodeSearch {




    public List<Integer> preorderTraversal(TreeNode root){
        List<Integer> result = new ArrayList<>();
        preorder(root,result);
        return result;
    }

    // 前序遍历·递归·LC144_二叉树的前序遍历 中左右
    public void preorder (TreeNode root, List<Integer> result){
        if (root == null){
            return;
        }
        result.add(root.val);
        preorder(root.left,result);
        preorder(root.right,result);
    }

    // 中序遍历·递归·LC94_二叉树的中序遍历  左中右
    public void inorder(TreeNode root,List<Integer> result){
        if (root == null){
            return;
        }
        inorder(root.left,result);
        // 注意这一句
        result.add(root.val);
        inorder(root.right,result);
    }

    // 后序遍历·递归·LC145_二叉树的后序遍历  左右中
    public void postorder(TreeNode root,List<Integer> result){
        if(root == null){
            return;
        }
        postorder(root.left,result);
        postorder(root.right,result);
        // 注意这一句
        result.add(root.val);
    }

    public static void main(String[] args) {
        TreeNode leftNode1 = new TreeNode(1, null, null);
        TreeNode rightNode1 = new TreeNode(2, null, null);

        TreeNode leftNode2 = new TreeNode(6, null, null);
        TreeNode rightNode2 = new TreeNode(8, null, null);

        TreeNode leftNode = new TreeNode(4, leftNode1, rightNode1);
        TreeNode rightNode = new TreeNode(7, leftNode2, rightNode2);

        TreeNode root = new TreeNode(5, leftNode, rightNode);

        TreeNodeSearch treeNodeSearch = new TreeNodeSearch();
        List<Integer> list = new ArrayList<>();
        treeNodeSearch.preorder(root,list);
        System.out.println("前序遍历: "+ list.toString());
    }
}
