package com.hy.treeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TreeNodeLevelOrder {

    /**
     * 102.二叉树的层序遍历
     * 力扣题目链接
     *
     * 给你一个二叉树，请你返回其按 层序遍历 得到的节点值。 （即逐层地，从左到右访问所有节点）。
     *
     * 层序遍历。
     *
     * 层序遍历一个二叉树。就是从左到右一层一层的去遍历二叉树。这种遍历的方式和我们之前讲过的都不太一样。
     *
     * 需要借用一个辅助数据结构即队列来实现，队列先进先出，符合一层一层遍历的逻辑，而是用栈先进后出适合模拟深度优先遍历也就是递归的逻辑。
     *
     * 而这种层序遍历方式就是图论中的广度优先遍历，只不过我们应用在二叉树上。
     */
    public List<List<Integer>> resList = new ArrayList<List<Integer>>();

    // BFS--迭代方式--借助队列
    public  List<List<Integer>> levelOrder(TreeNode root){
        LinkedList<TreeNode> queue = new LinkedList<>();
        if (root == null){
            return null;
        }
        queue.offer(root);

        while (!queue.isEmpty()){
            List<Integer> temp = new ArrayList<>();
            int len = queue.size();
            while (len > 0){
                // 弹出 元素
                TreeNode node = queue.poll();
                temp.add(node.val);

                if (node.left != null){
                    queue.offer(node.left);
                }
                if (node.right != null){
                    queue.offer(node.right);
                }
                len --;
            }
            resList.add(temp);
        }
        return resList;
    }

    //DFS--递归方式
    public void levelOrder2(TreeNode root,Integer deep){
        if (root == null){
            return;
        }
        deep ++;

        //当层级增加时，list的Item也增加，利用list的索引值进行层级界定
        if (resList.size() < deep){
            ArrayList<Integer> item = new ArrayList<>();
            resList.add(item);
        }
        // 添加元素
        resList.get(deep - 1).add(root.val);
        levelOrder2(root.left,deep);
        levelOrder2(root.right, deep);
    }






    public static void main(String[] args) {
        TreeNode leftNode1 = new TreeNode(1, null, null);
        TreeNode rightNode1 = new TreeNode(2, null, null);

        TreeNode leftNode2 = new TreeNode(6, null, null);
        TreeNode rightNode2 = new TreeNode(8, null, null);

        TreeNode leftNode = new TreeNode(4, leftNode1, rightNode1);
        TreeNode rightNode = new TreeNode(7, leftNode2, rightNode2);

        TreeNode root = new TreeNode(5, leftNode, rightNode);

        TreeNodeLevelOrder treeNodeLevelOrder = new TreeNodeLevelOrder();
        //System.out.println("层序遍历: "+treeNodeLevelOrder.levelOrder(root));

        treeNodeLevelOrder.levelOrder2(root,0);
        System.out.println("res: "+ treeNodeLevelOrder.resList);
    }
}
