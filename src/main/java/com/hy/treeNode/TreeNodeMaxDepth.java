package com.hy.treeNode;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class TreeNodeMaxDepth {


    /**
     * 104.二叉树的最大深度
     * 力扣题目链接
     *
     * 给定一个二叉树，找出其最大深度。
     *
     * 二叉树的深度为根节点到最远叶子节点的最长路径上的节点数。
     *
     * 说明: 叶子节点是指没有子节点的节点。
     *
     * 示例：
     * 给定二叉树 [3,9,20,null,null,15,7]，
     *
     * 返回它的最大深度 3 。
     *
     * 思路：
     *
     * 使用迭代法的话，使用层序遍历是最为合适的，因为最大的深度就是二叉树的层数，和层序遍历的方式极其吻合。
     *
     * 在二叉树中，一层一层的来遍历二叉树，记录一下遍历的层数就是二叉树的深度，如图所示：
     */
    public Integer getTreeNodeMaxDepth(TreeNode root){
        Deque<TreeNode> deque = new LinkedList<>();
        List<List<Integer>> res = new ArrayList<>();
        if (root == null){
            return 0;
        }
        deque.addLast(root);
        int depth = 0;
        while (!deque.isEmpty()){
            int len = deque.size();
            List<Integer> temp = new ArrayList<>();

            for (int i = 0; i < len; i++) {
                TreeNode node = deque.poll();
                temp.add(node.val);

                if (node.left != null){
                    deque.addLast(node.left);
                }
                if(node.right != null){
                    deque.addLast(node.right);
                }
            }
            depth++;
        }
        return depth;
    }

    public static void main(String[] args) {

        TreeNode leftNode1 = new TreeNode(1, null, null);
        TreeNode rightNode1 = new TreeNode(2, null, null);

        TreeNode leftNode2 = new TreeNode(6, null, null);
        TreeNode rightNode2 = new TreeNode(8, null, null);

        TreeNode leftNode = new TreeNode(4, leftNode1, rightNode1);
        TreeNode rightNode = new TreeNode(7, leftNode2, rightNode2);

        TreeNode root = new TreeNode(5, leftNode, rightNode);


        TreeNodeMaxDepth treeNodeMaxDepth = new TreeNodeMaxDepth();
        System.out.println("二叉树最大的深度： "+treeNodeMaxDepth.getTreeNodeMaxDepth(root));


    }
}
