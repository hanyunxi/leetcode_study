package com.hy.treeNode;

public class PerfectTreeNode {


    int val;
    PerfectTreeNode left;
    PerfectTreeNode right;
    PerfectTreeNode next;

    public PerfectTreeNode(int val) {
        this.val = val;
    }

    public PerfectTreeNode(int val, PerfectTreeNode left, PerfectTreeNode right, PerfectTreeNode next) {
        this.val = val;
        this.left = left;
        this.right = right;
        this.next = next;
    }

    @Override
    public String toString() {
        return "PerfectTreeNode{" +
                "val=" + val +
                ", left=" + left +
                ", right=" + right +
                ", next=" + next +
                '}';
    }
}
