package com.hy.node;


/**
 * 什么是链表，链表是一种通过指针串联在一起的线性结构，每一个节点由两部分组成，一个是数据域一个是指针域（存放指向下一个节点的指针），
 * 最后一个节点的指针域指向null（空指针的意思）。
 * 链接的入口节点称为链表的头结点也就是head。
 *
 * 单链表  node1 -> node2 -> node3 ->
 * 单链表中的指针域只能指向节点的下一个节点。
 *
 * 双链表
 * 单链表中的指针域只能指向节点的下一个节点。
 *
 * 双链表：每一个节点有两个指针域，一个指向下一个节点，一个指向上一个节点。
 *
 * 双链表 既可以向前查询也可以向后查询。
 *
 * 循环链表:
 *
 * 循环链表，顾名思义，就是链表首尾相连。
 * 循环链表可以用来解决约瑟夫环问题。
 *
 *  链表的存储方式:
 * 了解完链表的类型，再来说一说链表在内存中的存储方式。
 *
 * 数组是在内存中是连续分布的，但是链表在内存中可不是连续分布的。
 *
 * 链表是通过指针域的指针链接在内存中各个节点。
 *
 * 所以链表中的节点在内存中不是连续分布的 ，而是散乱分布在内存中的某地址上，分配机制取决于操作系统的内存管理。
 *
 *              插入、删除(时间复杂度)                    查询(时间复杂度)
 *   数组         o(n)  涉及元素移动              o(1)   通过索引小标取值
 *   链表         o(1)  更改指针指向              o(n)   通过iterator 遍历链表元素 equals 比较
 */
public class ListNode {
    // 节点的值
    int val;
    // 下一个节点
    ListNode next;

    public ListNode() {
    }

    public ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }

    public int getVal() {
        return val;
    }

    public void setVal(int val) {
        this.val = val;
    }

    public ListNode getNext() {
        return next;
    }

    public void setNext(ListNode next) {
        this.next = next;
    }

    public static ListNode iterator(ListNode head){
        ListNode pre = null,curr,next;
        curr = head;
        while (curr != null){
            next = curr.next;
            curr.next = pre;
            pre = curr;
            curr = next;
        }
        return pre;
    }

    @Override
    public String toString() {
        return "ListNode{" +
                "val=" + val +
                ", next=" + next +
                '}';
    }
}
