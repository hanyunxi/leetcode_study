package com.hy.node;

public class Test {
    int a = 0;
    boolean flag = false;

    public  void write() {
        a = 2;      //1
        flag = true;    //2
        System.out.println("a: "+a);
        System.out.println("flag: "+flag);
    }
    public  void multiply() {
        if (flag) {         //3
            int ret = a * a;//4
            System.out.println("ret: "+ ret);
        }
        System.out.println("flag: "+flag);
    }

    public static void main(String[] args) {
        Test test = new Test();
        test.write();
        test.multiply();



    }
}
