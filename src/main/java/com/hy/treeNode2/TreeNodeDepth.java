package com.hy.treeNode2;

import com.hy.treeNode.TreeNode;

import java.util.Deque;
import java.util.LinkedList;

public class TreeNodeDepth {


    /**
     * 二叉树 最大深度
     * 迭代法，使用层序遍历
     * @param root
     * @return
     */
    public int getTreeNodeMaxDepth(TreeNode root){
        Deque<TreeNode> deque = new LinkedList<>();
        if (root == null){
            return 0;
        }
        deque.offerLast(root);
        int depth = 0;
        while (!deque.isEmpty()){
            int len = deque.size();
            depth ++;
            for (int i = 0; i < len; i++) {
                TreeNode node = deque.pollFirst();
                if (node.left != null){
                    deque.offerLast(node.left);
                }
                if (node.right != null){
                    deque.offerLast(node.right);
                }
            }
        }
        return depth;
    }

    /**
     * 二叉树 最大深度
     * DFS 递归
     * @param root
     * @return
     */
    public int maxDepth2(TreeNode root){
        if (root == null){
            return 0;
        }
        int leftDepth = maxDepth2(root.left); // 左
        int rightDepth = maxDepth2(root.right); //右
        return Math.max(leftDepth,rightDepth) + 1; //中
    }

    /**
     * 二叉树- 最小深度
     * 迭代法，使用层序遍历
     * @param root
     * @return
     */
    public int getTreeNodeMinDepth(TreeNode root){
        Deque<TreeNode> deque = new LinkedList<>();
        if (root == null){
            return 0;
        }
        deque.offerLast(root);
        int depth = 0;
        while (!deque.isEmpty()){
            depth++;
            int len = deque.size();
            while (len > 0){
                TreeNode node = deque.pollFirst();
                // 如果当前节点的左右孩子都为空，直接返回最小深度
                if (node.left == null && node.right == null){
                    return depth;
                }
                if(node.left != null) {
                    deque.offerLast(node.left);
                }
                if (node.right != null){
                    deque.offerLast(node.right);
                }
                len --;
            }
        }
        return depth;
    }

    /**
     * 二叉树 最小深度
     *  递归法，相比求MaxDepth要复杂点
     *  因为最小深度是从根节点到最近**叶子节点**的最短路径上的节点数量
     * DFS 递归
     * @param root
     * @return
     */
    public int minDepth2(TreeNode root){
        if (root == null){
            return 0;
        }
        int leftDepth = minDepth2(root.left);
        int rightDepth = minDepth2(root.right);
        if (root.left == null){
           return rightDepth + 1;
        }
        if(root.right == null){
            return leftDepth + 1;
        }
        // 左右结点都不为null
        return Math.min(leftDepth,rightDepth) + 1;
    }

    public static void main(String[] args) {

        TreeNode leftNode3 = new TreeNode(8, null, null);
        TreeNode rightNode3 = new TreeNode(8, null, null);

        TreeNode leftNode2 = new TreeNode(4, null, null);
        TreeNode rightNode2 = new TreeNode(3, null, rightNode3);

        TreeNode leftNode1 = new TreeNode(3, leftNode3, null);
        TreeNode rightNode1 = new TreeNode(4, null, null);

        TreeNode leftNode = new TreeNode(2, leftNode1, rightNode1);
        TreeNode rightNode = new TreeNode(2, leftNode2, rightNode2);

        TreeNode root = new TreeNode(1, leftNode, rightNode);


        TreeNodeDepth treeNodeDepth = new TreeNodeDepth();


        System.out.println("二叉树最大深度（迭代）："+treeNodeDepth.getTreeNodeMaxDepth(root));
        System.out.println("二叉树最大深度(递归)："+treeNodeDepth.maxDepth2(root));
        System.out.println("二叉树最小深度（迭代）："+treeNodeDepth.getTreeNodeMinDepth(root));
        System.out.println("二叉树最小深度（递归）："+treeNodeDepth.minDepth2(root));

    }
}
