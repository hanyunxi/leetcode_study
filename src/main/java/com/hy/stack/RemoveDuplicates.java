package com.hy.stack;

import java.util.ArrayDeque;
import java.util.Stack;

public class RemoveDuplicates {


    /**
     * 栈与队列：匹配问题都是栈的强项
     * 1047. 删除字符串中的所有相邻重复项
     * 给出由小写字母组成的字符串 S，重复项删除操作会选择两个相邻且相同的字母，并删除它们。
     * 在 S 上反复执行重复项删除操作，直到无法继续删除。
     * 在完成所有重复项删除操作后返回最终的字符串。答案保证唯一。
     *
     * 示例：
     *
     * 输入："abbaca"
     * 输出："ca"
     * 解释：例如，在 "abbaca" 中，我们可以删除 "bb" 由于两字母相邻且相同，这是此时唯一可以执行删除操作的重复项。
     * 之后我们得到字符串 "aaca"，其中又只有 "aa" 可以执行重复项删除操作，所以最后的字符串为 "ca"。
     *
     * 思路
     * 题外话
     * 这道题目就像是我们玩过的游戏对对碰，如果相同的元素放在挨在一起就要消除。
     *
     * 可能我们在玩游戏的时候感觉理所当然应该消除，但程序又怎么知道该如果消除呢，特别是消除之后又有新的元素可能挨在一起。
     *
     * 此时游戏的后端逻辑就可以用一个栈来实现（我没有实际考察对对碰或者爱消除游戏的代码实现，仅从原理上进行推断）。
     *
     * 游戏开发可能使用栈结构，编程语言的一些功能实现也会使用栈结构，实现函数递归调用就需要栈，但不是每种编程语言都支持递归，
     *
     * 正题
     * 本题要删除相邻相同元素，其实也是匹配问题，相同左元素相当于左括号，相同右元素就是相当于右括号，匹配上了就删除。
     *
     * 那么再来看一下本题：可以把字符串顺序放到一个栈中，然后如果相同的话 栈就弹出，这样最后栈里剩下的元素都是相邻不相同的元素了。
     * @param s
     */
    public static String removeDuplicates(String s){
        ArrayDeque<Character> deque = new ArrayDeque<>();

        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            // 队列为空  或者 取出的队列元素 和当前元素不等  则 放入队列中
            if (deque.isEmpty() || deque.peek() != c){
                deque.push(c);
            }else {
            // 相等的话   则弹出元素
                deque.pop();
            }
        }
        String str = "";
        while (!deque.isEmpty()){
            str += deque.pop();
        }
        return str;
    }

    public static void main(String[] args) {
        String str = "abbacddca";

        System.out.println("res: "+ removeDuplicates(str));
    }
}
