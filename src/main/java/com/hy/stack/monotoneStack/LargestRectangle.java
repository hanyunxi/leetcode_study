package com.hy.stack.monotoneStack;

import java.util.Stack;

public class LargestRectangle {

    /**
     * 84.柱状图中最大的矩形
     * 力扣题目链接
     *
     * 给定 n 个非负整数，用来表示柱状图中各个柱子的高度。每个柱子彼此相邻，且宽度为 1 。
     *
     * 求在该柱状图中，能够勾勒出来的矩形的最大面积。
     *
     *
     * @param heights
     * @return
     */
    // 双指针法     如上代码并不能通过leetcode，超时了，因为时间复杂度是O(n2)。
    public static int largestRectangle(int [] heights){
        int len = heights.length;
        int sum = 0;
        for (int i = 0; i < len; i++) {
            int left = i;
            int right = i;
            // 两两比较 获取最大的柱子 索引
            for (; left >= 0; left--){
                if (heights[left] < heights[i]){
                    break;
                }
            }
            for (; right < len; right++){
                if (heights[right] < heights[i]){
                    break;
                }
            }
            int w = right - left - 1;
            int h = heights[i];
            sum = Math.max(sum,w * h);
        }
        return sum;
    }

    // 难就难在本题要记录记录每个柱子 左边第一个小于该柱子的下标，而不是左边第一个小于该柱子的高度。
    public static int largestRectangle02(int [] heights){
        int len = heights.length;
        int [] minLeftIndex = new int[len];
        int [] maxRigthIndex = new int[len];
        // 记录左边第一个小于该柱子的下标
        minLeftIndex[0] = -1;
        for (int i = 1; i < len; i++) {
            int t = i - 1;
            while (t >= 0 && heights[t] >= heights[i]){
                t = minLeftIndex[t];
            }
            minLeftIndex[i] = t;
        }
        // 记录每个柱子 右边第一个小于该柱子的下标
        maxRigthIndex[len - 1] = len;
        for (int i = len - 2; i >= 0; i--) {
            int t = i + 1;
            while (t < len && heights[t] >= heights[i]){
                t = maxRigthIndex[t];
            }
            maxRigthIndex[i] = t;
        }

        int res = 0;
        for (int i = 0; i < len; i++) {
            int sum = heights[i] * (maxRigthIndex[i] - minLeftIndex[i] - 1);
            res = Math.max(sum,res);
        }
        return res;
    }
    // 单调栈
    public static int largestRectangle03(int [] heights){
        Stack<Integer> st = new Stack<>();
        // 数组扩容，在头和尾各加入一个元素
        int [] newHeights = new int[heights.length +2];
        newHeights[0] = 0;
        newHeights[newHeights.length - 1] = 0;
        for (int i = 0; i < heights.length; i++) {
            newHeights[i + 1] = heights[i];
        }
        heights = newHeights;

        st.push(0);
        int res = 0;
        // 第一个元素已经入栈，从下标1开始
        for (int i = 1; i < heights.length; i++) {
            // 注意heights[i] 是和heights[st.top()] 比较 ，st.top()是下标
            if (heights[i] > heights[st.peek()]){
                st.push(i);
            }else if (heights[i] == heights[st.peek()]){
                // 这个可以加，可以不加，效果一样，思路不同
                st.pop();
                st.push(i);
            }else {
                // 注意是while
                while (heights[i] < heights[st.peek()]){
                    Integer mid = st.peek();
                    st.pop();
                    int left = st.peek();
                    int right = i;
                    int w = right - left - 1;
                    int h = heights[mid];
                    res = Math.max(res,w * h);
                }
                st.push(i);
            }
        }
        return res;
    }

    public static void main(String[] args) {
        int [] heights = {2,1,5,6,2,3};
        System.out.println("res: "+largestRectangle(heights));
        System.out.println("res: "+largestRectangle02(heights));
        System.out.println("res: "+largestRectangle03(heights));
    }
}
