package com.hy.stack;

import java.util.Stack;

public class MyQueue {

    /**
     * 栈与队列：我用栈来实现队列怎么样？
     * 232.用栈实现队列
     * 力扣题目链接
     *
     * 使用栈实现队列的下列操作：
     *
     * push(x) -- 将一个元素放入队列的尾部。
     * pop() -- 从队列首部移除元素。
     * peek() -- 返回队列首部的元素。
     * empty() -- 返回队列是否为空。
     *
     * 思路
     * 这是一道模拟题，不涉及到具体算法，考察的就是对栈和队列的掌握程度。
     *
     * 使用栈来模式队列的行为，如果仅仅用一个栈，是一定不行的，所以需要两个栈一个输入栈，一个输出栈，这里要注意输入栈和输出栈的关系。
     *
     * 下面动画模拟以下队列的执行过程如下：
     *
     * 执行语句：
     * queue.push(1);
     * queue.push(2);
     * queue.pop(); 注意此时的输出栈的操作
     * queue.push(3);
     * queue.push(4);
     * queue.pop();
     * queue.pop();注意此时的输出栈的操作
     * queue.pop();
     * queue.empty();
     */

    Stack<Integer> stackIn;
    Stack<Integer> stackOut;

    public MyQueue(){
        // 负责进栈
        stackIn = new Stack<>();
        // 负责出栈
        stackOut = new Stack<>();
    }

    // Push element x to the back of queue
    public void push(int x){
        stackIn.push(x);
    }
    // Removes the element from in front of queue and returns that element.
    public int pop (){
        dumpstackIn();
        return stackOut.pop();
    }
    //  Get the front element.
    public int peek(){
        dumpstackIn();
        return stackOut.peek();
    }
    // Returns whether the queue is empty.
    public boolean empty(){
        return stackIn.isEmpty() && stackOut.isEmpty();
    }

    public void dumpstackIn(){
        if (!stackOut.isEmpty()){
            return;
        }
        while (!stackIn.isEmpty()){
            stackOut.push(stackIn.pop());
        }
    }
    public static void main(String[] args) {
        MyQueue myQueue = new MyQueue();
        myQueue.push(1);
        myQueue.push(2);
        System.out.println("栈是否为空："+myQueue.empty());
        System.out.println("出栈数据1： "+myQueue.pop());
        myQueue.push(3);
        myQueue.push(4);
        System.out.println("出栈数据2： "+myQueue.pop());
        System.out.println("出栈数据3： "+myQueue.pop());
        System.out.println("获取栈中最前面的元素："+myQueue.peek());
        System.out.println("出栈数据4： "+myQueue.pop());
        System.out.println("栈是否为空："+myQueue.empty());

    }
}
